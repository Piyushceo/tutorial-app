import { Component, OnInit } from '@angular/core';
/* 
Ionic native HTTP module 
Angular's HttpClient module
*/
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-reg',
  templateUrl: './reg.page.html',
  styleUrls: ['./reg.page.scss'],
})
export class RegPage implements OnInit {
  
  firstname:string='';
  lastname:string='';
  phone:any='';
  password:string='';
  confirm:string='';
  gender:string='';
  response:any;
  constructor(public http:HttpClient) { }

  change()
  {
    //alert(this.gender);
  }
  submit()
  {
    if(this.phone.length!=10)
    alert("Phone number is not correct");
    else if (this.password!=this.confirm)
    alert("Both passwords are not the same");
    else
    {
      let url='',json={};
      json={'phone':this.phone,'firstname':this.firstname,'lastname':this.lastname,'gender':this.gender,'password':this.password};
      alert("Hurray! Your form has been successfully filled!");
      this.http.post(url,JSON.stringify(json))
      .subscribe((resp)=>this.response=resp,
      (err)=>alert(JSON.stringify(err)),
      ()=>{
        //do stuff here
      });
      this.http.get(url)
      .subscribe((resp)=>this.response=resp,
      (err)=>alert(JSON.stringify(err)),
      ()=>{
        //do stuff here
      });
      //backend integration 
    }
  }
  ngOnInit() {
  }

}
